This repository is to test your understanding of programming concepts and basic usage of Git repositories.
Clone the repo to your local system
Read the questions carefully, create your solutions in your own filename, and push.
NOTE show proper commenting and add your name to the top comment of the solution file.

Make a pull request, merge and push your code.
-----------------------------------------------------------------------------------

Q. A

Given the matrix

12 20 -4
4  50  5
11  8 10

Create a function that takes three array inputes as argument to form our array and 
returns the difference between the summation of the left diagonal and summation of the right diagonal.

given our example above, 

array1 = [12, 20, -4]
array2 = [4,  50,  5]
array3 = [11,  8, 10]

return value would be the difference bettwen (12 + 20 + -4) - (-4 + 50 + 11).

-----------------------------------------------------------------------------------

Q. B

Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four of the five integers. Then print the respective minimum and maximum values as a single line of two space-separated long integers.

Input Format

A single line of five space-separated integers.

Constraints

    Each integer is in the inclusive range .

Output Format

Print two space-separated long integers denoting the respective minimum and maximum values that can be calculated by summing exactly four of the five integers. (The output can be greater than a 32 bit integer.)

Sample Input

1 2 3 4 5

Sample Output

10 14

Explanation

Our initial numbers are 1, 2, 3, 4, and 5. We can calculate the following sums using four of the five integers:

    If we sum everything except 1, our sum is 2 + 3 + 4 + 5 = 14. 
    If we sum everything except 2, our sum is 1 + 3 + 4 + 5 = 13.
    If we sum everything except 3, our sum is 1 + 2 + 4 + 5 = 12.
    If we sum everything except 4, our sum is 1 + 2 + 3 + 5 = 11.
    If we sum everything except 5, our sum is 1 + 2 + 3 + 4 = 10.

-----------------------------------------------------------------------------------
