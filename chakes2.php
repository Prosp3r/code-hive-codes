<?php

$a = 1; $b = 2; $c = 3; $d = 4; $e = 5;

function sum($v, $w, $x, $y){
    $z = $v + $w + $x + $y;
    return $z;
}

$result1 = sum($b, $c, $d, $e);
$result2 = sum($a, $c, $d, $e);
$result3 = sum($a, $b, $d, $e);
$result4 = sum($a, $b, $c, $e);
$result5 = sum($a, $b, $c, $d);

echo "The minimum is: ";
echo(min($result1, $result2, $result3, $result4, $result5));
echo "<br>";
echo "The maximum is: ";
echo(max($result1, $result2, $result3, $result4, $result5));

?>